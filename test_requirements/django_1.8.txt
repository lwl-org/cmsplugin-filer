django>=1.8,<1.9
django-reversion>=1.8.2,<1.9
django-mptt>=0.7,<0.9
aldryn-apphook-reload>=0.2.2
djangocms-helper>=0.9.1
https://github.com/divio/django-filer/archive/develop.zip
djangocms-text-ckeditor
html5lib<0.99999999
django-polymorphic<0.8

-r base.txt